// alert("Hello!")


// Exponent Opearator

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8,2); // this is before our ES6
console.log(secondNum);

// 5 raised to the power of 5 
const thirdNum = 5 ** 5;
console.log(thirdNum);

// Template Literals

/*
	Allows us to write strings without using the concatenation operator (+) 
	
*/

let name = "Nehemiah";

// Pre-Template Literal String
// Using single quote ('')
let message = 'Hello ' + name + ' Welcome to programming!';
console.log("Message without template literals: " + message)

// Strings Using Template Literal
// Uses the backticks (``)

message = `Hello ${name}, Welcome to programming!`;
console.log(message);


let anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution ${firstNum}

`
console.log(anotherMessage);

anotherMessage = "\n" + name + 'attended a math competion. \n He won it by solving the problem 8 ** 2 with the solution' + firstNum + ". \n"
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings is: ${principal * interestRate}`);


// Array Destructuring:
/*
	Allows us to unpack elements in arrays into disctict variables. Allows us to name array elemenst with variables instead of index numbers.

	Syntax:
		let/const [variableName, variableName, variableName] = array;



*/



const fullName = ["Joe", "Dela", "Cruz"]


// Pre-Array Destructuring

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);


console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to see you.`)

// Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName)
console.log(middleName)
console.log(lastName)

console.log(`Hello, ${firstName} ${middleName} ${lastName}! It's nice to see you.`)


// Object Destructuring
/*
	Allows us to unpack properties of objects into distint variables. Shortens the syntax for accessing properties from objects.

	Syntax:
		let/const {propertyName, propertyName, propertyName} = object;


*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// Pre-Object Destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

const {maidenName, givenName, familyName} = person;
console.log(givenName)
console.log(maidenName)
console.log(familyName)

function getFullName ({givenName, maidenName, familyName}) {
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person)


// Arrow Functions
/*
		Alternative syntax to traditional functions

		const variableName = () => {
			console.log
		}
*/


const hello = () => {
	console.log("Hello World!");
}

hello()

function greeting () {
	console.log("Hello World!");
}

greeting()


// Pre-Arrow Function
/*
	Syntax:
		function functionName (parameterA, parameterB) {
			console.log()
		}

*/

function printFullName (firstName, middleInitial, lastName) {
	console.log(firstName + " " + middleInitial + " " + lastName);
}

printFullName("Amiel", "D.", "Oliva")

// Arrow Function
/*
	Syntax:
	 let/const variableName = (parameterA, parameterB) => {
			console.log()
	 }

*/


const printFullName1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printFullName("Charles Patrick", "A.", "Ilagan")


const students = ["Rupert", "Carlos", "Jerome"];
// Arrow Functions with loops

// Pre-Arrow Functions 

students.forEach(function (student) {
	console.log(`${student} is a student.`)
})



// Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student.`)
})

// Implicit Return Statement

// Pre-Arrow Function
function add(x, y) {
	return x + y
}

let total = add(12, 15);
console.log(total);

// Arrow Function
let addition = (x, y) => x + y; //may add return statement but make sure it's inside the curly braces

addition = (x, y) => {
	return x + y; 
}

let resultOfAdd = addition(12, 15);
console.log(resultOfAdd)


// arrow function cannot be applied in constractor function or ?constractor object function?


// Default Function Argument Value

const greet = (name = "User") => {
	return `Good morning, ${name}!`
}

console.log(greet())
console.log(greet("Grace"))


// Class-Based Object Blueprints/Template
/*
	Allows creation/instatiation of objects using classes as blueprints.

	Syntax:
		class ClassName {
			constructo (objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
	
*/


class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);


const myNewCar = new Car ("Toyota", "Vios", 2021);
console.log(myNewCar);

// OOP - object orient programming language in Java
// class constructor function is more complicated than reg constructor function


